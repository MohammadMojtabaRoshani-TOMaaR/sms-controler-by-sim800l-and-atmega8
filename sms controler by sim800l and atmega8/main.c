#include <mega8.h>
#include <stdio.h>
#include <string.h>
#include <delay.h>

const char enter=13,double_quote=34;
char send[128],receive[128];
int counter,i;
interrupt [USART_RXC] void usart_rx_isr(void)
  {
  receive[counter]=UDR;
  if(UDR==0x0A)counter=0;
  else counter++;
  }

void main(void)
  {
  DDRB=0x0f;
  UCSRA=(0<<RXC) | (0<<TXC) | (0<<UDRE) | (0<<FE) | (0<<DOR) | (0<<UPE) | (0<<U2X) | (0<<MPCM);
  UCSRB=(1<<RXCIE) | (0<<TXCIE) | (0<<UDRIE) | (1<<RXEN) | (1<<TXEN) | (0<<UCSZ2) | (0<<RXB8) | (0<<TXB8);
  UCSRC=(1<<URSEL) | (0<<UMSEL) | (0<<UPM1) | (0<<UPM0) | (0<<USBS) | (1<<UCSZ1) | (1<<UCSZ0) | (0<<UCPOL);
  UBRRH=0x00;
  UBRRL=0x33;
  #asm("sei")
  sprintf(send,"AT%c",enter);
  puts(send);
  delay_ms(1000);
  sprintf(send,"AT+CMGF=1%c",enter);
  puts(send);
  delay_ms(1000);
  sprintf(send,"AT+CMGDA=%cDEL ALL%c%c",double_quote,double_quote,enter);
  puts(send);
  delay_ms(1000);
  sprintf(send,"ATE0%c",enter);
  puts(send);
  delay_ms(1000);
  sprintf(send,"AT+CSQ%c",enter);
  puts(send);
  delay_ms(1000);
  while (1)
    {
    if(strstr(receive,"+CMTI: \"SM\",1"))
      {
      sprintf(send,"AT+CMGF=1%c",enter);
      puts(send);
      delay_ms(1000);
      sprintf(send,"AT+CMGR=1%c",enter);
      puts(send);
      delay_ms(1000);
      if(strstr(receive,"port1"))PORTB.0=~PORTB.0;
      if(strstr(receive,"port2"))PORTB.1=~PORTB.1;
      if(strstr(receive,"port3"))PORTB.2=~PORTB.2;
      if(strstr(receive,"port4"))PORTB.3=~PORTB.3;
      for(i=0;i<128;i++)receive[i]='\0';
      counter=0;
      delay_ms(1000);
      sprintf(send,"AT+CMGDA=%cDEL ALL%c%c",double_quote,double_quote,enter);
      puts(send);
      delay_ms(1000);
      }
    }
  }
