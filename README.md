# sms-controler-by-sim800l-and-atmega8

first, to do this project see sim800L data sheet on below link:

http://mt-system.ru/sites/default/files/documents/sim800_hardware_design_v1.09.pdf

(If link does not work you have to search on google).

then, you need these for your project:

1.ATmega8.
2.GSM sim800L.
3.Relay.
4.battery (Lithium-ion cuse the power supply should be between 4.2 and 3.8).

- I use USART so :

  RX -> to -> pin 3(TXD).
  TX -> to -> pin 2(RXD).

- Libraries:

  mega8.h   => for use DDRB and PORTB.
  mega8.h   => for use sprintf string configuration.
  string.h  => for strstr method (search string in another string).
  
  - Note:
     + Bit fuse must be on 8 MH. 
     + send massage by this text:
        * portX ( 0<= x <=3 ) [ no space between port and X ].
     + If the voltage is not supplied maybe you see an error!!.
  
See help for more Description!.
